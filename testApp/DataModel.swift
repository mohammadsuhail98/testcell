//
//  info.swift
//  testApp
//
//  Created by Newarpunk on 1/28/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation

class DataModel {
    enum StudentState {
        case none
        case present
        case absent
        case late
        case leave
    }
    
    var name : String
    var state : StudentState
    
    init(name : String, state : StudentState) {
        self.name = name
        self.state = state
    }
}
