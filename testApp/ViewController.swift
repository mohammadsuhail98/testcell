//
//  ViewController.swift
//  testApp
//
//  Created by Newarpunk on 1/28/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class ViewController: UIViewController, StudentStateDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var present : Int = 0
    var absent : Int = 0
    var late : Int = 0
    var leave : Int = 0
    
    var allData = DataArray()
    
    var selectedData = [String]()
    
    // For attendance Counting
    @IBOutlet weak var presentClicked: UIButton!
    @IBOutlet weak var absentClicked: UIButton!
    @IBOutlet weak var lateClicked: UIButton!
    @IBOutlet weak var leaveClicked: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }

    @IBAction func totalCountClicked(_ sender: UIButton) {
        tableView.reloadData()
        let alert = UIAlertController(title: "Count", message: "Total Present: \(present) \n Total Absent: \(absent) \n Total late: \(late) \n On Leave: \(leave)", preferredStyle: .alert)
        let addAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(addAction)
        self.present(alert, animated: true, completion: nil)
//        print("selectedData are \(selectedData)")
    }
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let totalCount = allData.dataSource.count
        return totalCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        let model = allData.dataSource[indexPath.item]
        cell.cellConfigure(dataModel: model)
        cell.delegateValue = self
        cell.indexPath = indexPath
        return cell
    }
    
    func valueHere(countValue num : Int) {
        present = num
        absent = num
        late = num
        leave = num
    }
    
    func buttonClicked(state: DataModel.StudentState, indexPath: IndexPath?) {
        if let indexPath = indexPath {
            let model = allData.dataSource[indexPath.item]
            model.state = state
            print("\(state) button is selected")
            // print("\(indexPath) is seleceted")
            
            // Count function here
            let totalCount = allData.dataSource.count
            
            for _ in 0...totalCount {
                if state == .present {
                    present += 1
                    return
                } else if state == .absent {
                    absent += 1
                    return
                } else if state == .late {
                    late += 1
                    return
                } else if state == .leave {
                    leave += 1
                    return
                }
                return
            }
        }
    }
}

//for i in 0..<totalCount {
//                if i == indexPath.item{
//                    if state == .present {
////                        valueHere(countValue: indexPath.item)
//                        selectedData.append("present")
////                        if selectedData.count > 1 {
////                            if selectedData.last == "present" {
////                                selectedData.removeLast()
////                            }
////                        }
//
//
////                        present += 1
////                        print(present)
//                        return
//                    } else if state == .absent {
////                        valueHere(countValue: indexPath.item)
//                        selectedData.append("absent")
////                        absent += 1
////                        print(absent)
//                        return
//                    } else if state == .late {
////                        valueHere(countValue: indexPath.item)
//                        selectedData.append("late")
////                        late += 1
////                        print(late)
//                        return
//                    } else if state == .leave {
////                        valueHere(countValue: indexPath.item)
//                        selectedData.append("leave")
////                        leave += 1
////                        print(leave)
//                        return
//                    }
//                    return
//                }
//            }
