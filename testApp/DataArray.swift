//
//  dataArray.swift
//  testApp
//
//  Created by Newarpunk on 1/28/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation

class DataArray {
    var dataSource : [DataModel] = []
    
    init() {
        for i in 0...15 {
            dataSource.append(DataModel(name: "akash \(i)", state: .none))
        }
    }
}
